<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    public function albums(){
        return $this->belongsToMany('App\Album');
    }

    public function songs(){
        return $this->hasMany('App\Song');
    }

    public function labels(){
        return $this->belongsToMany('App\Label');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function tags(){
        return $this->belongsToMany('App\Tag');
    }

    public function concerts(){
        return $this->belongsToMany('App\Concert');
    }

    public function images(){
        return $this->hasMany('App\Image');
    }
}
