<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Label extends Model
{
    public function artist(){
        return $this->belongsToMany('App\Artist');
    }

    public function album(){
        return $this->belongsTo('App\Album');
    }
}
