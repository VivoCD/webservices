<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    public function user(){
        return $this->hasOne('App\User');
    }

    public function songs(){
        return $this->belongsToMany('App\Song');
    }
}
