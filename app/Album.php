<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    public function artists(){
        return $this->belongsToMany('App\Artist');
    }

    public function songs(){
        return $this->hasMany('App\Song');
    }

    public function label(){
        return $this->hasOne('App\Label');
    }

    public function tags(){
        return $this->belongsToMany('App\Tag');
    }
}
