<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function artists(){
        return $this->belongsToMany('App\Artist');
    }

    public function albums(){
        return $this->belongsToMany('App\Album');
    }
}
