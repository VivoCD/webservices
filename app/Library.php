<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Library extends Model
{
    public function user(){
        return $this->hasOne('App\User');
    }

    public function artists(){
        return $this->belongsToMany('App\Artist');
    }

    public function albums(){
        return $this->belongsToMany('App\Album');
    }

}
