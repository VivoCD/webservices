<?php

namespace App\Http\Controllers;

use App\Album;
use App\Artist;
use App\Comment;
use App\Concert;
use App\Image;
use App\Label;
use App\Song;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DataController extends Controller
{
    /*----------------------------------------------------------
        * Récupération de l'ensemble des données
     */
    public function getAll(){
        $artists = Artist::with('labels','concerts', 'albums', 'images', 'comments')->get();

        // Init collection de retour
        $allArtists = collect();

        // Traitement
        foreach($artists as $artist){

            /*----------------------------------------------------
                * Initialisation des collections
             * */
            $oneArtist = collect();

            $allProps = collect();
            $allAlbums = collect();
            $allImages = collect();
            $allComments = collect();
            $allConcerts = collect();

            /*----------------------------------------------------
                * Récupération des informations
             */
            // Objet contenant les informations basique de l'artiste
            $artistDatas = $this->attributes($artist->getAttributes());
            $artistDatas->label = $artist->labels->first()->name;
            $artistDatas->images = $allImages;
            //$artistDatas->image = $artist->images->first()->link;
            $artistDatas->albums = $allAlbums;
            //$artistDatas->comments = $allComments;
            $artistDatas->concerts = $allConcerts;

            // Ajout dans les propriétés de l'artiste
            $allProps->push($artistDatas);

            // Récupération des images de l'artiste
            foreach($artist->images as $image){
                $oneImage = $this->attributes($image->getAttributes());
                $allImages->push($oneImage);
            }

            // Récupération des infos d'albums
            foreach($artist->albums as $album){
                // Init collection musiques
                $allSongs = collect();
                $albumName = $album->name;

                $oneAlbum = $this->attributes($album->getAttributes());
                $oneAlbum->label = $artist->labels->first()->name;
                //$oneAlbum->comments

                // Récupération des musiques
                foreach($album->songs as $song){
                    $oneSong = $this->attributes($song->getAttributes());
                    $oneSong->file = asset(Storage::url($this->formatName($artist->name, 'nn').'/'.$this->formatName($albumName, 'nn').'/'.$this->formatName($song->title, 'nn').'.mp3'));
                    $allSongs->push($oneSong);
                }
                $oneAlbum->songs = $allSongs;

                // Récupération des commentaires
                foreach($artist->comments as $comment){
                    $oneComment = $this->attributes($comment->getAttributes());
                    $allComments->push($oneComment);
                }
                $oneAlbum->comments = $allComments;

                $allAlbums->push($oneAlbum);
            }

            // Récupération des commentaires
            /*foreach($artist->comments as $comment){
                $oneComment = $this->attributes($comment->getAttributes());
                $allComments->push($oneComment);
            }*/

            // Récupération des concerts
            foreach($artist->concerts as $concert){
                $oneConcert = $this->attributes($concert->getAttributes());
                $allConcerts->push($oneConcert);
            }

            // Ajout dans la collection de l'artist courant
            //$oneArtist->push($allProps);

            $allArtists->push($artistDatas);
        }
        return response()->json($allArtists);
    }

    /*----------------------------------------------------------
        * Dispatcher pour demande globale
     */
    public function globalDispatcher($type, $query){
        $method = 'get'.ucfirst($type);

        return $this->{$method}($query);
    }

    /*----------------------------------------------------------
        * Fonction pour demandes globale
     */
    public function getArtists($name){
        /*----------------------------------------------------------
            * Traitement du nom de l'artiste
            * Récupération du nom de la fonction
         */
        $name = $this->formatName($name, null);
        $funcName = $this->getName(__FUNCTION__);

        $artists = $this->makeQuery($funcName, $name);

        $res = collect();
        $allImages = collect();
        $allComments = collect();
        $allTags = collect();

        foreach($artists as $artist){
            $oneArtist = $this->attributes($artist->getAttributes());

            $oneArtist->label = $artist->label = $artist->labels->first()->name;
            $oneArtist->images = $allImages;
            $oneArtist->comments = $allComments;
            $oneArtist->tags = $allTags;

            foreach($artist->images as $image){
                $oneImage = $this->attributes($image->getAttributes());
                $allImages->push($oneImage);
            }

            foreach($artist->comments as $comment){
                $oneComment = $this->attributes($comment->getAttributes());
                $allComments->push($oneComment);
            }

            foreach($artist->tags as $tag){
                $oneTag = $this->attributes($tag->getAttributes());
                $allTags->push($oneTag->content);
            }

            $res->push($oneArtist);
        }



        return response()->json($res);
    }

    public function getAlbums($name){
        $name = $this->formatName($name, null);
        $funcName = $this->getName(__FUNCTION__);

        $albums = $this->makeQuery($funcName, $name);

        $res = collect();
        $allSongs = collect();
        $allTags = collect();

        foreach($albums as $album){
            $oneAlbum = $this->attributes($album->getAttributes());
            $oneAlbum->songs = $allSongs;
            $oneAlbum->tags = $allTags;

            foreach($album->songs as $song){
                $oneSong = $this->attributes($song->getAttributes());
                $allSongs->push($oneSong);
            }

            foreach($album->tags as $tag){
                $oneTag = $this->attributes($tag->getAttributes());
                $allTags->push($oneTag->content);
            }

            $res->push($oneAlbum);
        }

        return response()->json($res);
    }

    public function getSongs($name){
        $funcName = $this->getName(__FUNCTION__);

        $songs = $this->makeQuery($funcName, $this->formatName($name, null));

        $res = collect();

        $artist = $this->formatName($songs->first()->album->first()->artists->first()->name, 'nn');
        $album = $this->formatName($songs->first()->album->first()->name, 'nn');

        foreach($songs as $song) {
            $oneSong = $this->attributes($song->getAttributes());
            //dd($song->title);
            $oneSong->file = asset($artist.'/'.$album.'/'.$this->formatName($song->title, 'nn').'.mp3');
            $res->push($oneSong);
        }

        return response()->json($res);
    }

    public function getConcerts($name){
        $name = $this->formatName($name, null);
        $funcName = $this->getName(__FUNCTION__);

        $concerts = $this->makeQuery($funcName, $name);

        $res = collect();

        foreach($concerts as $concert){
            $oneConcert = $this->attributes($concert->getAttributes());
            $res->push($oneConcert);
        }

        return response()->json($res);
    }

    /*----------------------------------------------------------
        * Fonction pour demandes spécifiques
     */
    public function getArtist($name){
        /*----------------------------------------------------------
            * Traitement du nom de l'artiste
            * Récupération du nom de la fonction
         */
        $name = explode('_',$name);

        $name = array_map(function($word){
             return ucfirst($word);
        }, $name);

        $name = implode(' ', $name);

        $funcName = $this->getName(__FUNCTION__);

        /*----------------------------------------------------------
            * Récupération des informations
         */
        $artist = $this->makeQuery($funcName, $name);

        /*----------------------------------------------------------
            * Init collections
         */
        $res = collect();
        $allImages = collect();
        $allAlbums = collect();
        $allTags = collect();

        $artProp = $this->attributes($artist->getAttributes());
        $artProp->label = $artist->label = $artist->labels->first()->name;

        foreach($artist->images as $image){
            $oneImage = $this->attributes($image->getAttributes());
            $allImages->push($oneImage);
        }

        foreach($artist->albums as $album){
            $oneAlbum = $this->attributes($album->getAttributes());
            $allAlbums->push($oneAlbum);
        }

        foreach($artist->tags as $tag){
            $oneTag = $this->attributes($tag->getAttributes());

            $allTags->push($oneTag);
        }

        $artProp->images = $allImages;
        $artProp->albums = $allAlbums->unique('name');
        $artProp->tags = $allTags->unique('content');

        $res->push($artProp);

        return response()->json($res);
    }

    public function getAlbum($name){
        /*----------------------------------------------------------
            * Traitement du nom de l'artiste
            * Récupération du nom de la fonction
         */
        $name = explode('_',$name);

        $name = array_map(function($word){
            return ucfirst($word);
        }, $name);

        $name = implode(' ', $name);

        $funcName = $this->getName(__FUNCTION__);

        /*----------------------------------------------------------
            * Récupération des informations
         */
        $album = $this->makeQuery($funcName, $name);

        /*----------------------------------------------------------
            * Init collections
         */
        $res = collect();
        $allSongs = collect();

        $artist = $album->artists->first()->name;
        $labelName = $album->artists->first()->labels->first()->name;
        $albumName = $album->name;

        $albProp = $this->attributes($album->getAttributes());
        $albProp->artist = $artist;
        $albProp->label = $labelName; // Temp à refaire
        $albProp->songs = $allSongs;

        foreach($album->songs as $song){
            $oneSong = $this->attributes($song->getAttributes());
            //$oneSong->file = asset($this->formatName($artist, 'nn').'/'.$this->formatName($albumName, 'nn').'/'.$this->formatName($song->title, 'nn').'.mp3');
            $oneSong->file = asset(Storage::url($this->formatName($artist, 'nn').'/'.$this->formatName($albumName, 'nn').'/'.$this->formatName($song->title, 'nn').'.mp3'));
            $allSongs->push($oneSong);
        }

        $res->push($albProp);

        return response()->json($res);
    }

    public function getConcert($name, $date){
        /*----------------------------------------------------------
            * Traitement du nom de l'artiste
            * Traitement de la date
            * Récupération du nom de la fonction
         */
        $name = explode('_',$name);

        $name = array_map(function($word){
            return ucfirst($word);
        }, $name);

        $name = implode(' ', $name);

        $date = Carbon::createFromFormat('d_m_Y', $date)->toDateString();

        $funcName = $this->getName(__FUNCTION__);

        $toQuery = new \stdClass();
        $toQuery->name = $name;
        $toQuery->date = $date;

        /*----------------------------------------------------------
            * Récupération des informations
         */
        $concert = $this->makeQuery($funcName, $toQuery);

        /*----------------------------------------------------------
            * Init collections
         */
        $res = collect();

        $ConProp = $this->attributes($concert->getAttributes());

        $res->push($ConProp);
        return response()->json($res);
    }

    public function getSong($artist, $album, $title){
        //dd(asset('storage/test.html'));
        dd(asset($artist.'/'.$album.'/'.$title.'.mp3'));
        //dd(Storage::url($artist.'/'.$album.'/'.$title.'.mp3'));
    }

    public function getLastArtists($name){

    }

    /*----------------------------------------------------------
        * Helpers
     */
    public function attributes($attrs){
        $obj = new \stdClass();
        foreach($attrs as $attribute => $value){
            if($attribute !== 'updated_at' && $attribute !== 'created_at'){
                $obj->{$attribute} = $value;
            }

            if ($attribute == 'release' || $attribute == 'date'){
                $obj->{$attribute} = Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
            }
        }

        return $obj;
    }

    public function makeQuery($dType, $toSearch){
        $res = null;

        switch ($dType){
            case 'artists':
                $res = Artist::with('labels','concerts', 'albums', 'images', 'comments')->get();
                break;
            case 'artist':
                $res = Artist::with('labels','concerts', 'albums', 'images', 'comments')
                    ->where('name', $toSearch)->first();
                break;
            case 'albums':
                $res = Album::with('artists', 'songs', 'tags')
                    ->whereHas('artists', function($query) use($toSearch){
                        $query->where('name', $toSearch);
                    })->get()->sortByDesc('release');
                break;
            case 'album':
                $res = Album::with('artists', 'songs')->where('name', $toSearch)->first();
                break;
            case 'concerts':
                $res = Concert::with('artists')->whereHas('artists', function($query) use($toSearch){
                    $query->where('name', $toSearch);
                })->get();
                break;
            case 'concert':
                $res = Concert::with('artists')
                    ->whereHas('artists', function($query) use($toSearch){
                       $query->where('name', $toSearch->name);
                    })->where('date', $toSearch->date)->first();
            case 'songs':
                $res = Song::with('album.artists')->whereHas('album', function($query) use($toSearch){
                    $query->where('name', $toSearch);
                })->get();
        }

        return $res;
    }

    public function getName($name){
        $type = preg_match("/[A-Z][a-z]{2,}/", $name, $match);

        return strtolower($match[0]);
    }

    public function formatName($name, $type){
        if(is_null($type)){
            $name = explode('_',$name);
        }else{
            $name = explode(' ',$name);
        }

        $name = array_map(function($word) use($type){
            if(is_null($type)){
                return ucfirst($word);
            } else{
                return strtolower($word);
            }
        }, $name);

        if(is_null($type)){
            return implode(' ', $name);
        }else{
            return implode('_', $name);
        }

    }
}
