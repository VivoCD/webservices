<?php

namespace App\Http\Controllers;

use App\Album;
use App\Artist;
use App\Comment;
use App\Concert;
use App\Image;
use App\Label;
use App\Song;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class FeedController extends Controller
{
    /*----------------------------------------------------------
        * Ajout en base
    */
    public function add(Request $request){
        $content = json_decode($request->input('content'));

        // Récupération des tags existants
        $artists = Artist::where('name', $content->name)->get();

        // Ajout d'un artist
        if(!$artists->isEmpty()){
            $artist = $artists->first();
        }else{
            $artist = new Artist;
            $artist->name = $this->formatName($content->name, 'notDir');
            $artist->shortname = $content->shortname;
            $artist->country = $content->country;

            $artist->is_ended = $content->life->ended;
            $artist->begin = $content->life->begin;
            $artist->end = $content->life->end;
            $artist->save();
        }

        // Nom dossier artist
        $artDir = $this->formatName($artist->name, 'dir');

        // Création du dossier de l'artist
        /*if(!in_array('public/'.$artDir, Storage::directories('/public/'))){
            Storage::makeDirectory('/public/'.$artDir);
        }*/
        $this->makeDir(null, $artDir);

        // Gestion des images

        foreach($content->images as $image){
            $images = Image::where('link', $image->link)->get();

            if($images->isEmpty()){
                $newImage = new Image;

                $newImage->link = $image->link;
                $newImage->artist_id = $artist->id;

                $newImage->save();
            }

        }

        // Gestion des albums
        foreach($content->albums as $album){
            $albums = Album::where(['name' => $album->name, 'artist_id' => $artist->id])->get();

            if(!$albums->isEmpty()){
                $newAlbum = $albums->first();
            }else{
                $newAlbum = new Album;

                $newAlbum->name = $this->formatName($album->name, 'notDir');

                // Traitement de la date
                $release = Carbon::createFromFormat('d/m/Y', $album->release)->toDateTimeString();
                $newAlbum->release = $release;

                $newAlbum->style = $this->formatName($album->style, 'notDir');
                $newAlbum->cover = $album->cover;
                $newAlbum->artist_id = $artist->id;
            }

            // Vérification de l'existance du label
            $labels = Label::where('name', $content->label)->get();

            if(!$labels->isEmpty()){
                $label = $labels->first();
                $label->artist()->attach($artist->id);
                $newAlbum->label_id = $label->id;
            }else{
                $newLabel = new Label;

                $newLabel->name = $this->formatName($content->label, 'notDir');
                $newLabel->save();
                $newLabel->artist()->attach($artist->id);
                $newAlbum->label_id = $newLabel->id;
            }

            $newAlbum->save();

            // Nom dossier artist
            $albDir = $this->formatName($album->name, 'dir');
            $this->makeDir($artDir, $albDir);

            $newAlbum->artists()->attach($artist->id);

            // Gestion des tags
            foreach($content->tags as $tag){
                // Vérification de l'existance du tag
                $tags = Tag::where('content', $tag)->get();

                if(!$tags->isEmpty()){
                    $tags = $tags->first();
                    $tags->artists()->attach($artist->id);
                    $tags->albums()->attach($newAlbum->id);

                }else{
                    $newTag = new Tag;

                    $newTag->content = $this->formatName($tag, 'notDir');
                    $newTag->save();
                    $newTag->artists()->detach($artist->id);
                    $newTag->artists()->attach($artist->id);
                    $newTag->albums()->detach($newAlbum->id);
                    $newTag->albums()->attach($newAlbum->id);

                }
            }
            // Gestion des pistes
            foreach($album->songs as $song){
                $songs = Song::where(['title' => $song->title, 'album_id' => $newAlbum->id])->get();

                if($songs->isEmpty()){
                    $newSong = new Song;

                    $newSong->title = $this->formatName($song->title, 'notDir');
                    $newSong->duration = $song->duration;
                    $newSong->album_id = $newAlbum->id;

                    $newSong->save();
                }
            }

            // Gestion des commentaires
            foreach($content->comments as $comment){
                $comments = Comment::where(['name' => $comment->name, 'author' => $comment->author, 'artist_id' => $artist->id])->get();

                if($comments->isEmpty()){
                    $newComment = new Comment;

                    $newComment->name = $comment->name;
                    $newComment->author = $this->formatName($comment->author, 'notDir');

                    // Traitement de la date
                    $publication = Carbon::createFromFormat('d/m/Y', $comment->publication)->toDateTimeString();
                    $newComment->posted = $publication;

                    $newComment->content = $comment->content;
                    $newComment->resume = $comment->resume;
                    $newComment->artist_id = $artist->id;

                    $newComment->save();
                }
            }
        }

        // Gestion des concerts
        foreach($content->concerts as $concert){
            $concerts = Concert::with('artists')->whereHas('artists', function ($query) use($artist){
                $query->where('artists.id', $artist->id);
            })->where('city', $concert->city)->get();

            if(!$concerts->isEmpty()){
                $newConcert = $concerts->first();
            }else{
                $newConcert = new Concert;

                // Traitement de la date
                $dateC = Carbon::createFromFormat('d/m/Y', $concert->dateC)->toDateTimeString();
                $newConcert->date = $dateC;

                $newConcert->city = $concert->city;
                $newConcert->place = $concert->place;
                $newConcert->address = $concert->address;
                $newConcert->is_full = $concert->full;

                $newConcert->save();
            }


            $newConcert->artists()->detach($artist->id);
            $newConcert->artists()->attach($artist->id);
        }

        return '<h3>Données ajoutée</h3>';

    }

    /*----------------------------------------------------------
        * Helper
    */
    public function formatName($name, $type){
        $name = explode(' ',$name);

        $name = array_map(function($word) use($type){
            if($type !== 'dir'){
                return ucfirst($word);
            } else{
                return strtolower($word);
            }
        }, $name);

        if($type !== 'dir'){
            return implode(' ', $name);
        }else{
            return implode('_', $name);
        }

    }

    public function makeDir($parent, $name){
        if(is_null($parent)){
            if(!in_array('public/'.$name, Storage::directories('/public/'))){
                Storage::makeDirectory('/public/'.$name);
            }
        }else{
            if(!in_array('public/'.$parent.'/'.$name, Storage::directories('/public/'.$parent))){
                Storage::makeDirectory('public/'.$parent.'/'.$name);
            }
        }
    }
}
