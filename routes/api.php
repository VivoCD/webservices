<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['prefix' => 'feed'], function () {
    Route::post('add', 'FeedController@add')->name('add');
});

Route::group(['prefix' => 'datas'], function(){
    Route::get('all', 'DataController@getAll')->name('all');

    Route::get('/global/{type}/{query}', 'DataController@globalDispatcher')->name('global');

    Route::get('/artist/{name}', 'DataController@getArtist')->name('artist');
    Route::get('/album/{name}', 'DataController@getAlbum')->name('album');
    Route::get('/concert/{artist}/{date}', 'DataController@getConcert')->name('concert');
    Route::get('/song/{artist}/{album}/{title}', 'DataController@getSong')->name('song');
});